package com.example.nikhil.springbatchtest.batch;

import com.example.nikhil.springbatchtest.model.User;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class Processor implements ItemProcessor<User,User> {

    private static final Map<String,String> USER_MAP = new HashMap<>();

    public Processor() {
        USER_MAP.put("001", "Technology");
        USER_MAP.put("002", "Operations");
        USER_MAP.put("003", "Accounts");
    }

    @Override
    public User process(User user) throws Exception {
        String userDeptId = user.getDept();
        String userDeptName = USER_MAP.get(userDeptId);
        user.setDept(userDeptName);
        return user;
    }
}
