package com.example.nikhil.springbatchtest.batch;

import com.example.nikhil.springbatchtest.model.User;
import com.example.nikhil.springbatchtest.repository.UserRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Writer implements ItemWriter<User> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void write(List<? extends User> users) throws Exception {
        System.out.println("here are all the users" +users);
        userRepository.saveAll(users);
    }
}
